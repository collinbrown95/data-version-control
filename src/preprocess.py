"""
A preprocessing script that runs some basic image preprocessing on the raw data.

Moves the raw data to the `data/train` folder.
"""
import os
import cv2

RAW_PATH = "data/raw"
TRAIN_PATH = "data/train"


def main():
    """
    Simple main method to implement image preprocessing.
    """
    for img_file in os.listdir(RAW_PATH):
        # Load image
        img = cv2.imread(
            os.path.join(os.path.dirname(__file__), "..", RAW_PATH, img_file)
        )
        img = cv2.resize(img, dsize=(64, 64))
        cv2.imwrite(
            os.path.join(os.path.dirname(__file__), "..", TRAIN_PATH, img_file), img
        )


if __name__ == "__main__":
    main()
