# data-version-control

Exploring Data Version Control (DVC) and related version control tools

## Workflow Concepts

- Whenever there is an atomic change in the state of the data (i.e. we want to define a new "checkpoint" that we can roll back to):

1. Add the data to dvc `dvc add <path to file or folder>` - dvc will automatically add this location to a `.gitignore` file in the folder containing the file or folder added by dvc. DVC will also add a `<filename.dvc>` file in the same location as the file or folder being tracked by dvc.
2. Create a git commit containing the additions of the `*.dvc` files along with any relevant other changes in source code, documentation, etc.
3. Push your changes to your dvc registry of choice (e.g. `dvc push -r local-registry`) - this creates a "checkpoint" in the dvc registry that is associated with the state of your repository at the time of the commit. The reason to do this is that if you `git checkout` to the commit you just made, you want dvc to associate the state of the data with the `*.dvc` files that are present in that commit.
4. Push your changes to your git remote and add a tag to the commit you just pushed - this will associate a semantically meaningful tag to the commit you want to associate with a particular dvc push, and you can include a written description here with details to refer back to later.


If you do the above for several iterations, you should have a history of tags associated with your repository. If you run `git fetch origin` then `git tag` you will see a list of all the tags that are associated with your repo. To revert your repository to the state of a particular tag, do `git checkout tags/<tag name you want to checkout>`. For example, `git checkout tags/2021-05-11-initial-data` will revert the repository to the commit associated with the tag `2021-05-11-initial-data`.

Once you revert to the state of the repository you want, use `dvc pull` to get the appropriate files from your data registry (e.g. `dvc pull -r local-registry`). Since the repository contains exactly the `*.dvc` files that were present at the time of the commit, `dvc` knows exactly which files to place in your local data folder.

If you want to checkout to another commit (or go back to the `HEAD` of the branch you were working on), just `git checkout <your branch>` and run `dvc pull` again, and dvc will restore the data files associated with the current commit.


## DVC Configuration

**Disable Anonymous Usage Analytics**

```bash
dvc config core.analytics false
```

## Resources

https://devconnected.com/how-to-checkout-git-tags/
